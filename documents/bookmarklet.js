/******************************************
*   Robert Koteles
*   Web developer
*   2017
******************************************/

javascript:void (function() {
    "use strict";

    var pageBody,
          tbl,
          tblBody,
          divContainer;

      function generateInfo() {  
        divContainer = document.createElement("div");
        divContainer.setAttribute("id", "microdataInfoBox");

        tbl = document.createElement("table");
        tblBody = document.createElement("tbody");    
      }

      function appendNewRow(title, content, merge) {
        var row = document.createElement("tr");

        var cell1 = document.createElement("td");
        //style of cell
        cell1.style.background = "#F1F1F1";
        cell1.style.fontWeight = "bold";
        cell1.appendChild( document.createTextNode( title ) );
        cell1.style.padding = "10px";
        cell1.style.verticalAlign = "top";
        if( merge ) {
          cell1.colSpan = "2";
        }
        row.appendChild(cell1);

        if( !merge ) {
          var cell2 = document.createElement("td");
          //style of cell
          cell2.appendChild( document.createTextNode( content ) );
          cell2.style.padding = "10px";
          cell2.style.verticalAlign = "top";
          row.appendChild(cell2);
        } 
        
        //append this new row to <tbody>
        tblBody.appendChild(row);
      };

      function appendInfoToDom() {
        // insert the <tbody> in the <table>
        tbl.appendChild(tblBody);
        // append <table> into <div>
        divContainer.appendChild(tbl);
        // append <div> into <body>
        pageBody.appendChild(divContainer);
      };

      function styleOfInfo() {
        //style container
        divContainer.style.position = "fixed";
        divContainer.style.bottom = 0;
        divContainer.style.right = 0;
        divContainer.style.width = "50%";     
        divContainer.style.height = "400px";
        divContainer.style.overflow = "auto"; 
        divContainer.style.border = "5px solid #ecc96b";  
        divContainer.style.zIndex = "10000";   
        divContainer.style.backgroundColor  = "#FFFFFF";   

        //style table
        tbl.style.width = "100%";
        tbl.style.border = "1px solid #F1F1F1";
        tbl.style.fontSize = "14px";
      }


    function startInspect() {
      
      pageBody = document.getElementsByTagName("body").item(0);

      //if script been already used, not necessary to use again
      if (document.getElementById("microdataInfoBox")) {
        console.log("Info box is exist!");
        return false;
      }
      
      generateInfo();
      
      //itemtype="http://schema.org/Product"
      var firstElement = document.querySelectorAll('[itemtype="http://schema.org/Product"]').item(0);
      var otherElements = firstElement.querySelectorAll('[itemprop]');

      for( var i = 0; i < otherElements.length-1; i++) {
        if( otherElements[i].tagName ) { //prevention of handling not valid objects such as ___prototype
          
          if( otherElements[i].querySelectorAll('[itemprop]').length ) {
            //This element has more children having itemprop attribute so just put it in the table as a merged header
            //console.log("it is a container: " + i);
            appendNewRow(otherElements[i].attributes.itemprop.value, '', true); 
          } else {
            //real name-value pair so should put in the table
            var content = otherElements[i].innerText;
            appendNewRow(otherElements[i].attributes.itemprop.value, content, false);  
          }

        }
      }

      appendInfoToDom();
      styleOfInfo();

    }

    startInspect();

})();