bookmarklet.js: this is the development version of my script
bookmarklet.min.js: minified version
bookmarklet-script.js: this is the ready-to-use version of the bookmarklet, compiled from the minified version of the script (less code, less size).